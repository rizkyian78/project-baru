'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: "Please Input Password"
        },
        isEmail: {
          msg: "Please Input Email"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: "Please Input Password"
        }
      }
    }
  }, {
    hooks: {
      beforeValidate: (instance, option) => {
        instance.email = instance.email.toLowerCase() 
      },
      beforeCreate: (instance, option) => {
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(instance.password, salt);
        instance.password = hashedPassword;
      }
    }
  });
  users.associate = function(models) {
    // associations can be defined here
    users.hasMany(models.task, {
      foreignKey: 'user_id',
      as: 'author'
    })
    users.hasOne(models.profile, {
      foreignKey: 'user_id',
      as: 'owner'
    })
  };
  return users;
};