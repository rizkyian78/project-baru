module.exports = (tasks) => {
    return tasks.sort((a,b) => {
        let dateA = new Date(a.dueDate);
        let dateB = new Date(b.dueDate)
        return dateB - dateA;
    })
}