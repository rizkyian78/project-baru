const router = require('express').Router();
const uploader = require('./Middleware/uploader');

const userController = require('./Controller/userController');
const taskController = require('./Controller/taskController');
const authenticate = require('./Middleware/authenticate');
const checkOwnership = require('./Middleware/checkOwnership');

// Task API
router.post('/tasks', authenticate, taskController.create); //done
router.get("/tasks",authenticate, taskController.show); //done
router.get('/tasks/filter/completion', authenticate, taskController.filteredCompletion); //done
router.get('/tasks/filter/importance', authenticate, taskController.filteredImportance); //done
router.put('/tasks/:id',authenticate, checkOwnership('task'), taskController.update); //done
router.put('/tasks/toogle-completion/:id', authenticate, checkOwnership('task'), taskController.toogleCompletion)
router.put('/tasks/toogle-importance/:id', authenticate, checkOwnership('task'), taskController.toogleImportance)
router.delete('/tasks/:id',authenticate, checkOwnership('task'), taskController.delete); //done

// User API
router.post('/user/register', userController.register); //done
router.post('/user/login', userController.login); //done
router.get('/user/profile', authenticate, userController.profile)
router.put('/user/update/:user_id', authenticate, uploader.single('image'), userController.update); //done 
// Buat image

module.exports = router