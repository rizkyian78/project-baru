const {tasks} = require('../models');

exports.paginated = (req, res, next) => {
    try {
        const task = tasks.findAll();

        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        const result = task.slice(startIndex, endIndex);
        next(result)
     } catch (err) {
         res.status(400).json({
             status: "Fail",
             Message: "Can't Found What you looking"
         })
     }
}