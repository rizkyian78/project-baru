const {users, profile} = require('../models');
const bcrypt = require('bcrypt');

const successHandler = require('../Helper/successHandler');
const errorHandler = require('../Helper/errorHandler');
const token = require('../Helper/token');
const imagekit = require('../lib/imagekit');

class User {
    static async register(req, res){
        try {
            const user = await users.create(req.body); //email dan pasword
            const profiles = await profile.create({
                name: req.body.name,
                user_id: user.id
            })
            successHandler(res, 201, {token: token(user)});
        } catch (err) {
            errorHandler(res, 400, err.message);
        };
    };
    static async login(req, res){
        try {
            const user = await users.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            });
            const isPasswordTrue = await bcrypt.compare(req.body.password, user.password);
            if(!user) errorHandler(res, 402, `${req.body.email} doesn't exist`);
            if(!isPasswordTrue) return errorHandler(res, 402, "Password doesn't Match");
            successHandler(res, 201, {token: token(user)});
        } catch (err) {
            errorHandler(res, 401, err.message);
        };
    };
    static async profile(req, res) {
        try {
            const data = await users.findByPk(req.user.id, {
                include: 'owner'
            });
            successHandler(res, 200, data)
        } catch (err) {
            throw new Error('Not Yours')
        }
    }
    static async update(req, res) {
        try {
            const image = await imagekit.upload({
                file: req.file.buffer,
                fileName: req.file.originalname
            })
            await profile.update({
                name: req.body.name,
                image: image.url    
            },{
                where: {id: req.params.id}
            },{
                include: 'owner'    
            })
            successHandler(res, 201, `id: ${req.params.id} has been updated`);
        } catch (err) {
            errorHandler(res, 402, "Please Insert the file");
        };
    };
};

module.exports = User;