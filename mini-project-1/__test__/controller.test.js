const request = require('supertest');
const app = require('../index');
const {task} = require('../models');

describe('Task API Collection ', () => {
    beforeAll(() => {
        return task.destroy({
            truncate: true
        })
    })
})